<?php
ini_set("memory_limit","256M");

require_once('ImageManipulator.php');
require_once('Poster.php');

// settings dummy
$destinationPath = '';
$image_name = (isset($_GET['img'])) ? $_GET['img'] : 'nasgor.jpg';
$name_dish = (isset($_GET['dish'])) ? $_GET['dish'] : 'Nasi Goreng Venny';
$shop_name = (isset($_GET['shop'])) ? $_GET['shop'] : 'Venny Angkringan';
$address = (isset($_GET['address'])) ? $_GET['address'] : 'Medan';

/* GENERATE POSTER - BEGIN */
// setelah simpan file asli, lakukan cropping
$im = new ImageManipulator($destinationPath.$image_name);

$width = $im->getWidth();
$height = $im->getHeight();

$limit_pic = $height;
if($width < $height){
	$limit_pic = $width;
}

// set max size for image
$standard = round($limit_pic / 2);

$centreX = round($im->getWidth() / 2);
$centreY = round($im->getHeight() / 2);
$x1 = $centreX - $standard;
$y1 = $centreY - $standard;
$x2 = $centreX + $standard;
$y2 = $centreY + $standard;

$im->crop($x1, $y1, $x2, $y2);

/*
$info = getimagesize($image_name);
$extension = image_type_to_extension($info[2]);
$image_name = str_ireplace($extension, '', $image_name).'.png';
*/

// cropped-masakan
$cropped_masakan = $destinationPath.'cropped-'.$image_name;
$im->save($cropped_masakan);

$cropped_img = new ImageManipulator($cropped_masakan);

$frame = 'frame.png';
$frame_img = new ImageManipulator($frame);
$frame_size = $frame_img->getWidth();

if($cropped_img->getWidth() != $frame_size){
    // resize image
    $cropped_resize = Poster::resize_image($cropped_masakan, $frame_size, $frame_size);
	
	// save tmp frame
	$cropped_tmp = 'image'.time().'.png';
	imagepng($cropped_resize, $cropped_tmp);
	imagedestroy($cropped_resize);
	
	$cropped_masakan = $cropped_tmp;
}

// tempel frame ke masakan menjadi poster
$dest = imagecreatefrompng($cropped_masakan);
$src = imagecreatefrompng($frame);
imagecopymerge ( $dest, $src, 0, 0, 0, 0, $frame_size, $frame_size, 100 );

// add text to poster
$dest = Poster::addTextToPoster($dest, $name_dish, $shop_name.' | '.$address);

// display poster in browser
header('Content-Type: image/png');
imagepng($dest);

/*
// save poster
list ($width, $height, $type) = getimagesize($cropped_masakan);
switch ($type) {
	case IMAGETYPE_GIF  :
		imagegif($dest, $destinationPath.'poster-'.$image_name);
		break;
	case IMAGETYPE_JPEG :
		imagejpeg($dest, $destinationPath.'poster-'.$image_name);
		break;
	case IMAGETYPE_PNG  :
		imagepng($dest, $destinationPath.'poster-'.$image_name);
		break;
	default             :
		throw new InvalidArgumentException("Image type $type not supported");
}
*/

imagedestroy($dest);
imagedestroy($src);

// remove tmp file
if(isset($cropped_tmp)){
	unlink( $cropped_masakan );
}
/* GENERATE POSTER - END */

?>