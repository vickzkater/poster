<?php

class Poster
{
    /*
	 * resizing frame (PNG) for poster image
	 */
	public static function resize_frame($file, $w, $h, $crop=FALSE) {
		list($width, $height) = getimagesize($file);
		$r = $width / $height;
		if ($crop) {
			if ($width > $height) {
				$width = ceil($width-($width*abs($r-$w/$h)));
			} else {
				$height = ceil($height-($height*abs($r-$w/$h)));
			}
			$newwidth = $w;
			$newheight = $h;
		} else {
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
		}
		$src = imagecreatefrompng($file);
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagealphablending($dst, false);
		imagesavealpha($dst, true);
		$transparent = imagecolorallocatealpha($dst, 255, 255, 255, 127);
		imagefilledrectangle($dst, 0, 0, $newwidth, $newheight, $transparent);
		
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		return $dst;
	}

	/*
	 * convert image PNG (usually 32bit) to PNG transparent (8bit)
	 */
	public static function convertPNGto8bitPNG($sourcePath, $destPath){
		$srcimage = imagecreatefrompng($sourcePath);
		list($width, $height) = getimagesize($sourcePath);
		
		$img = imagecreatetruecolor($width, $height);
		$bga = imagecolorallocatealpha($img, 0, 0, 0, 127);
		imagecolortransparent($img, $bga);
		imagefill($img, 0, 0, $bga);
		imagecopy($img, $srcimage, 0, 0, 0, 0, $width, $height);
		imagetruecolortopalette($img, false, 255);
		imagesavealpha($img, true);
		
		imagepng($img, $destPath);
		imagedestroy($img);
	}

	/*
	 * Adding text to image
	 */
	public static function addTextToPoster($image, $title, $subtitle){
		// Create Image From Existing File
		// $jpg_image = imagecreatefromjpeg($image);
		$jpg_image = $image;

		// Allocate A Color For The Text
		$white = imagecolorallocate($jpg_image, 255, 255, 255);
		$black = ImageColorAllocate($jpg_image, 0, 0, 0);

		// Set Path to Font File
		$font = 'lemonismDEMO.otf';

		// Set Text to Be Printed On Image
		$text = $title;

		// THE IMAGE SIZE
		$width = imagesx($jpg_image);
		$height = imagesy($jpg_image);

		// THE TEXT SIZE
		$font_sizes = [80, 52, 46, 42, 40, 36, 32, 28, 22, 18];

		// CENTERING THE TEXT BLOCK - Get X
		foreach($font_sizes as $font_size_title){
			$text_size = imagettfbbox($font_size_title, 0, $font, $text);
			$text_width = max([$text_size[2], $text_size[4]]) - min([$text_size[0], $text_size[6]]);
			$text_height = max([$text_size[5], $text_size[7]]) - min([$text_size[1], $text_size[3]]);
			
			$centerX = round(($width - $text_width) / 2);
			
			if($centerX > 150){
				break;
			}
		}

		// CENTERING THE TEXT BLOCK - Get Y
		$centerY = round(($height - $text_height) / 2);
		$centerY = $centerY<0 ? 0 : $centerY;
		// set agar berlokasi di banner
		$centerY += round($centerY / 2) + 30;

		// Print Text On Image
		// imagettftext ( resource $image , float $size , float $angle , int $x , int $y , int $color , string $fontfile , string $text )
		imagettftext($jpg_image, $font_size_title, 0, $centerX, $centerY, $white, $font, $text);

		// Set Path to Font File
		$font = 'DINPro-Medium_13936.ttf';

		// Set Text to Be Printed On Image
		$text = $subtitle;

		// THE TEXT SIZE
		$font_sizes = [28, 18, 14, 12, 10, 8];

		// CENTERING THE TEXT BLOCK - Get X
		foreach($font_sizes as $font_size_subtitle){
			$text_size = imagettfbbox($font_size_subtitle, 0, $font, $text);
			$text_width = max([$text_size[2], $text_size[4]]) - min([$text_size[0], $text_size[6]]);
			$text_height = max([$text_size[5], $text_size[7]]) - min([$text_size[1], $text_size[3]]);
			
			$centerX = round(($width - $text_width) / 2);
			
			if($centerX > 150){
				break;
			}
		}

		$centerY -= $text_height - 15;

		$centerX = round(($width - $text_width) / 2);
		$centerX = $centerX<0 ? 0 : $centerX;

		imagettftext($jpg_image, $font_size_subtitle, 0, $centerX, $centerY, $white, $font, $text);

		return $jpg_image;

		// Clear Memory
		// imagedestroy($jpg_image);
	}
	
	/*
	 * resizing image for poster image
	 */
	public static function resize_image($file, $w, $h, $crop=FALSE) {
		list($width, $height, $type) = getimagesize($file);
		$r = $width / $height;
		if ($crop) {
			if ($width > $height) {
				$width = ceil($width-($width*abs($r-$w/$h)));
			} else {
				$height = ceil($height-($height*abs($r-$w/$h)));
			}
			$newwidth = $w;
			$newheight = $h;
		} else {
			if ($w/$h > $r) {
				$newwidth = $h*$r;
				$newheight = $h;
			} else {
				$newheight = $w/$r;
				$newwidth = $w;
			}
		}
		
		switch ($type) {
        	case IMAGETYPE_GIF  :
        		$src = imagecreatefromgif($file);
        		break;
        	case IMAGETYPE_JPEG :
        		$src = imagecreatefromjpeg($file);
        		break;
        	case IMAGETYPE_PNG  :
        		$src = imagecreatefrompng($file);
        		break;
        	default             :
        		throw new InvalidArgumentException("Image type $type not supported");
        }
		
		
		$dst = imagecreatetruecolor($newwidth, $newheight);
		imagealphablending($dst, false);
		imagesavealpha($dst, true);
		$transparent = imagecolorallocatealpha($dst, 255, 255, 255, 127);
		imagefilledrectangle($dst, 0, 0, $newwidth, $newheight, $transparent);
		
		imagecopyresampled($dst, $src, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

		return $dst;
	}
	    
}
?>