<?php

require_once('ImageManipulator.php');
require_once('poster.php');

// settings dummy
$destinationPath = '';
$image_name = (isset($_GET['img'])) ? $_GET['img'] : 'nasgor.jpg';
$name_dish = (isset($_GET['dish'])) ? $_GET['dish'] : 'Nasi Goreng Venny';
$shop_name = (isset($_GET['shop'])) ? $_GET['shop'] : 'Venny Angkringan';
$address = (isset($_GET['address'])) ? $_GET['address'] : 'Medan';

/* GENERATE POSTER - BEGIN */
// setelah simpan file asli, lakukan cropping
$im = new ImageManipulator($destinationPath.$image_name);

$width = $im->getWidth();
$height = $im->getHeight();

$limit_pic = $height;
if($width < $height){
	$limit_pic = $width;
}

// set max size for image
$max_size = 1080;
if($limit_pic > $max_size){
	$limit_pic = $max_size;
}
$standard = round($limit_pic / 2);

$centreX = round($im->getWidth() / 2);
$centreY = round($im->getHeight() / 2);
$x1 = $centreX - $standard;
$y1 = $centreY - $standard;
$x2 = $centreX + $standard;
$y2 = $centreY + $standard;

$im->crop($x1, $y1, $x2, $y2);

// cropped-masakan
$cropped_masakan = $destinationPath.'cropped-'.$image_name;
$im->save($cropped_masakan);

// setting frame
$frame = 'frame.png';
$frame_img = new ImageManipulator($frame);
if($limit_pic < $frame_img->getWidth()){
	// resize frame
	$frame_resize = Poster::resize_frame($frame, $limit_pic, $limit_pic);
	
	// save tmp frame
	$frame_tmp = 'frame'.time().'.png';
	imagepng($frame_resize, $frame_tmp);
	imagedestroy($frame_resize);
	
	Poster::convertPNGto8bitPNG($frame_tmp, $frame_tmp);
	$frame = $frame_tmp;
}

// tempel frame ke masakan menjadi poster
$dest = imagecreatefromjpeg($cropped_masakan);
$src = imagecreatefrompng($frame);
imagecopymerge ( $dest, $src, 0, 0, 0, 0, $limit_pic, $limit_pic, 100 );

// add text to poster
$dest = Poster::addTextToPoster($dest, $name_dish, $shop_name.' | '.$address);

// display poster in browser
header('Content-Type: image/png');
imagepng($dest);

/*
// save poster
list ($width, $height, $type) = getimagesize($cropped_masakan);
switch ($type) {
	case IMAGETYPE_GIF  :
		imagegif($dest, $destinationPath.'poster-'.$image_name);
		break;
	case IMAGETYPE_JPEG :
		imagejpeg($dest, $destinationPath.'poster-'.$image_name);
		break;
	case IMAGETYPE_PNG  :
		imagepng($dest, $destinationPath.'poster-'.$image_name);
		break;
	default             :
		throw new InvalidArgumentException("Image type $type not supported");
}
*/

imagedestroy($dest);
imagedestroy($src);

// remove tmp frame
if(isset($frame_tmp)){
	unlink( $frame_tmp );
	unlink( $cropped_masakan );
}
/* GENERATE POSTER - END */

?>