# Poster

Generate poster by merge image with frame then add custom text.

Implemented in [Sahabat Unilever Food Solutions - Ngulik Rasa](https://www.sahabatufs.com/ngulikrasa/) (Aug 2019)

## Sample
![poster](https://gitlab.com/vickzkater/poster/raw/master/poster.png)

## Params for customize poster

- `img` = file name of image
- `dish` = text for dish name
- `shop` = text for shop name
- `address` = text for location

## Environment

- [x] PHP 5.6.40 & GD Version: bundled (2.1.0 compatible)
- [ ] PHP 7.2.10 & GD Version: bundled (2.1.0 compatible) - `NOT WORK (no image created)`
- [ ] PHP 7.2.19 & GD library Version: 2.2.5 - `NOT WORK (image blurry)`

## How to Use

- If want to using custom image, please copy the image in this project folder
- Then, access in browser using URL with some params (sample: `http://localhost/poster/?img=nasgor.jpg&dish=Chicken%20Baked%20Rice&shop=Kinidi%20Cafe&address=Pademangan`)

## Changelogs

- 2019/09/02 : poster_v2 release (image resizes following frame)
- 2019/08/02 : poster_v1 release (frame resizes following image)